package cross_shard

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/hub/client-indexer/clients/utxos/ec"
	"gitlab.com/jaxnet/hub/client-indexer/clients/utxos/utxo"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txmodels"
	"strings"
)

type SignatureFunc func(address string) (pubKey, signature string, err error)

type UTXOClient struct {
	clients       map[uint32]*fasthttp.Client
	signatureFunc SignatureFunc
}

func New(urls map[uint32]string, sigFunc SignatureFunc) (c *UTXOClient, err error) {

	hostClients := make(map[uint32]*fasthttp.Client)
	for shardID, url := range urls {
		addrParts := strings.Split(url, "://")
		if len(addrParts) < 2 {
			err = errors.New("wrong url data: " + url)
			return
		}
		var hostName string
		if addrParts[0] == "http" {
			hostName = addrParts[1]
		} else if addrParts[0] == "https" {
			hostName = addrParts[1] + ":443"
		} else {
			err = errors.New("invalid protocol name: " + addrParts[0])
			return
		}

		hostClients[shardID] = &fasthttp.Client{
			Name:                          hostName,
			DisableHeaderNamesNormalizing: true,
			DisablePathNormalizing:        true,
		}
	}
	c = &UTXOClient{
		clients:       hostClients,
		signatureFunc: sigFunc,
	}

	return
}

// SelectForAmount returns all found UTXOs for required `amount`, `shardID` and list of addresses.
// The list of UTXOs returned is sorted from the smallest one to the biggest one.
func (c *UTXOClient) SelectForAmount(amount int64, shardID uint32, addresses ...string) (utxos txmodels.UTXORows, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, address)
		if err != nil {
			return
		}

		utxos = append(utxos, fetchedUTXOs...)
	}

	return
}

// GetForAmount returns at most one UTXO for required `amount`, `shardID` and list of addresses.
// The list of UTXOs returned is sorted from the smallest one to the biggest one.
func (c *UTXOClient) GetForAmount(amount int64, shardID uint32, addresses ...string) (utxos *txmodels.UTXO, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, address)
		if err != nil {
			return
		}

		if len(fetchedUTXOs) > 0 {
			return &fetchedUTXOs[0], nil
		}
	}

	return
}

func (c *UTXOClient) GetByHashes(shardID uint32, address string, hashes ...string) (utxos txmodels.UTXORows, err error) {
	if len(hashes) == 0 {
		err = ec.ErrInvalidHashesList
		return
	}

	client, ok := c.clients[shardID]
	if !ok {
		err = errors.New("not supported shard ID")
		return
	}

	var url string
	if strings.Contains(client.Name, ":443") {
		url = fmt.Sprint("https://", client.Name, "/api/v1/indexes/utxos/non-spent/by-hash/")
	} else {
		url = fmt.Sprint("http://", client.Name, "/api/v1/indexes/utxos/non-spent/by-hash/")
	}

	query := fmt.Sprint(url, "?address=", address)
	for _, hash := range hashes {
		query = fmt.Sprint(query, "&tx_hash=", hash)
	}

	var (
		statusCode int
		body       []byte
	)

	statusCode, body, err = client.Get(nil, query)
	if err != nil {
		return
	}

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%d, %w", statusCode, ec.ErrInvalidIndexResponse)
		return
	}

	var receivedUTXOs []utxo.UTXO
	err = json.Unmarshal(body, &receivedUTXOs)
	if err != nil {
		return
	}

	for _, receivedUTXO := range receivedUTXOs {
		transformedUTXO := txmodels.UTXO{
			ShardID:    shardID,
			Address:    address,
			TxHash:     receivedUTXO.TxHash,
			OutIndex:   uint32(receivedUTXO.TxOutput),
			Value:      receivedUTXO.Amount,
			Used:       false,
			PKScript:   receivedUTXO.PKScript,
			ScriptType: "", //"pubkeyhash",
		}

		utxos = append(utxos, transformedUTXO)
	}

	return
}

func (c *UTXOClient) fetchUTXOsForAddress(amount int64, shardID uint32, address string) (utxos txmodels.UTXORows, err error) {
	// Note: no need to check arguments validity.
	// This validation is delegated to the API itself.
	// In worst case - error would be returned.

	pubKeyStr, sigStr, err := c.signatureFunc(address)
	if err != nil {
		return
	}

	client, ok := c.clients[shardID]
	if !ok {
		err = errors.New("not supported shard ID")
		return
	}

	var url string
	if strings.Contains(client.Name, ":443") {
		url = fmt.Sprint("https://", client.Name, "/api/v1/indexes/utxos/non-spent/one/")
	} else {
		url = fmt.Sprint("http://", client.Name, "/api/v1/indexes/utxos/non-spent/one/")
	}

	query := fmt.Sprint(url, "?address=", address, "&amount=", amount, "&pub_key=", pubKeyStr, "&signature=", sigStr)

	var (
		statusCode int
		body       []byte
	)

	statusCode, body, err = client.Get(nil, query)

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%s, %w", err, ec.ErrInvalidIndexResponse)
		return
	}
	if err != nil {
		return
	}

	var receivedUTXOs []utxo.UTXO
	err = json.Unmarshal(body, &receivedUTXOs)
	if err != nil {
		return
	}

	for _, receivedUTXO := range receivedUTXOs {
		transformedUTXO := txmodels.UTXO{
			ShardID:    shardID,
			Address:    address,
			TxHash:     receivedUTXO.TxHash,
			OutIndex:   uint32(receivedUTXO.TxOutput),
			Value:      receivedUTXO.Amount,
			Used:       false,
			PKScript:   receivedUTXO.PKScript,
			ScriptType: "", //"pubkeyhash",
		}

		utxos = append(utxos, transformedUTXO)
	}

	return
}
