package biggest_first

import (
	"gitlab.com/jaxnet/hub/client-indexer/clients/utxos/abstract"
)

type UTXOProvider struct {
	*abstract.APIClient
}

func New(url string, sigFunc abstract.SignatureFunc) (c *UTXOProvider, err error) {
	client, err := abstract.NewAPIClient(url, abstract.StrategyBiggestFirst, sigFunc)
	if err != nil {
		return
	}

	c = &UTXOProvider{
		APIClient: client,
	}

	return
}
