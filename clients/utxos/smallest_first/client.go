package smallest_first

import (
	"gitlab.com/jaxnet/hub/client-indexer/clients/utxos/abstract"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txmodels"
)

type UTXOProvider struct {
	*abstract.APIClient
}

func New(url string, sigFunc abstract.SignatureFunc) (c *UTXOProvider, err error) {
	client, err := abstract.NewAPIClient(url, abstract.StrategySmallestFirst, sigFunc)
	if err != nil {
		return
	}

	c = &UTXOProvider{
		APIClient: client,
	}

	return
}

func (c *UTXOProvider) NonSpentUTXOsLessEqualThanAmount(shardID uint32, address string, amount int64) (
	utxos txmodels.UTXORows, err error) {

	return c.FetchUTXOsForAddressLessEqualThanAmount(amount, shardID, address)
}
