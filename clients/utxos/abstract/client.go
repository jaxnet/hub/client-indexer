package abstract

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/hub/client-indexer/clients/utxos/ec"
	"gitlab.com/jaxnet/hub/client-indexer/clients/utxos/utxo"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txmodels"
	"strings"
)

const (
	StrategySmallestFirst = "smallest-first"
	StrategyBiggestFirst  = "biggest-first"
)

type SignatureFunc func(address string) (pubKey, signature string, err error)

type UTXOClient struct {
	client        *fasthttp.Client
	baseURL       string
	strategy      string
	signatureFunc SignatureFunc
}

func NewAbstractUTXOClient(url string, baseURL string, strategy string, sigFunc SignatureFunc) (c *UTXOClient, err error) {
	if strategy != StrategySmallestFirst && strategy != StrategyBiggestFirst {
		err = ec.ErrInvalidStrategy
		return
	}

	addrParts := strings.Split(url, "://")
	if len(addrParts) < 2 {
		err = errors.New("wrong url data: " + url)
		return
	}
	var hostName string
	if addrParts[0] == "http" {
		hostName = addrParts[1]
	} else if addrParts[0] == "https" {
		hostName = addrParts[1] + ":443"
	} else {
		err = errors.New("invalid protocol name: " + addrParts[0])
		return
	}

	c = &UTXOClient{
		strategy: strategy,
		baseURL:  baseURL,
		client: &fasthttp.Client{
			Name:                          hostName,
			DisableHeaderNamesNormalizing: true,
			DisablePathNormalizing:        true,
		},
		signatureFunc: sigFunc,
	}

	return
}

// SelectForAmount returns all found UTXOs for required `amount`, `shardID` for the first address from list of addresses and locks them for a few blocks.
func (c *UTXOClient) SelectForAmount(amount int64, shardID uint32, addresses ...string) (utxos txmodels.UTXORows, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var offset = 0
	var limit = 100
	var utxosSum int64
	var indexerResponse txmodels.UTXORows

	for ok := true; ok; ok = utxosSum < amount && len(indexerResponse) == limit {
		indexerResponse, err = c.fetchUTXOsForAddress(amount, shardID, offset, limit, addresses[0], true)
		if err != nil {
			return
		}

		if len(indexerResponse) == 0 {
			break
		}

		for _, u := range indexerResponse {
			utxosSum += u.Value
		}

		utxos = append(utxos, indexerResponse...)
	}

	return
}

// SelectForAmountWithoutLock returns all found UTXOs for required `amount`, `shardID` fot the first address of list of addresses.
func (c *UTXOClient) SelectForAmountWithoutLock(amount int64, shardID uint32, addresses ...string) (utxos txmodels.UTXORows, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var offset = 0
	var limit = 100
	var utxosSum int64
	var indexerResponse txmodels.UTXORows

	for ok := true; ok; ok = utxosSum < amount && len(indexerResponse) == limit {
		indexerResponse, err = c.fetchUTXOsForAddress(amount, shardID, offset, limit, addresses[0], false)
		if err != nil {
			return
		}

		if len(indexerResponse) == 0 {
			break
		}

		for _, u := range indexerResponse {
			utxosSum += u.Value
		}

		utxos = append(utxos, indexerResponse...)
		offset += limit
	}

	return
}

// GetForAmount returns at most one UTXO for required `amount`, `shardID` and list of addresses.
// The list of UTXOs returned is sorted from the smallest one to the biggest one.
func (c *UTXOClient) GetForAmount(amount int64, shardID uint32, addresses ...string) (utxos *txmodels.UTXO, err error) {
	if len(addresses) == 0 {
		err = ec.ErrInvalidAddressList
		return
	}

	var fetchedUTXOs txmodels.UTXORows
	for _, address := range addresses {
		fetchedUTXOs, err = c.fetchUTXOsForAddress(amount, shardID, 0, 1, address, true)
		if err != nil {
			return
		}

		if len(fetchedUTXOs) > 0 {
			return &fetchedUTXOs[0], nil
		}
	}

	return
}

func (c *UTXOClient) NonSpentUTXOsCnt(address string) (count uint64, err error) {
	query := fmt.Sprint(c.baseURL, "count/", "?address=", address)

	var (
		statusCode int
		body       []byte
	)
	statusCode, body, err = c.client.Get(nil, query)

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%s, %w", err, ec.ErrInvalidIndexResponse)
		return
	}
	if err != nil {
		return
	}

	type CountResponse struct {
		Count uint64 `json:"count"`
	}

	var receivedUTXOsCnt CountResponse

	err = json.Unmarshal(body, &receivedUTXOsCnt)
	if err != nil {
		return
	}

	count = receivedUTXOsCnt.Count
	return
}

func (c *UTXOClient) fetchUTXOsForAddress(amount int64, shardID uint32, offset, limit int, address string, useLock bool) (
	utxos txmodels.UTXORows, err error) {
	// Note: no need to check arguments validity.
	// This validation is delegated to the API itself.
	// In worst case - error would be returned.

	query := fmt.Sprint(c.baseURL, "?strategy=", c.strategy, "&address=", address, "&amount=", amount)
	if limit > 0 {
		query = fmt.Sprint(query, "&offset=", offset, "&limit=", limit)
	}

	if useLock {
		pubKeyStr, sigStr, err := c.signatureFunc(address)
		if err != nil {
			return nil, err
		}

		query = fmt.Sprint(query, "&pub_key=", pubKeyStr, "&signature=", sigStr)
	}

	var (
		statusCode int
		body       []byte
	)
	statusCode, body, err = c.client.Get(nil, query)
	if err != nil {
		return
	}

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%d, %w", statusCode, ec.ErrInvalidIndexResponse)
		return
	}

	var receivedUTXOs []utxo.UTXO
	err = json.Unmarshal(body, &receivedUTXOs)
	if err != nil {
		return
	}

	for _, receivedUTXO := range receivedUTXOs {
		transformedUTXO := txmodels.UTXO{
			ShardID:    shardID,
			Address:    address,
			TxHash:     receivedUTXO.TxHash,
			OutIndex:   uint32(receivedUTXO.TxOutput),
			Value:      receivedUTXO.Amount,
			Used:       false,
			PKScript:   receivedUTXO.PKScript,
			ScriptType: "", //"pubkeyhash",
		}

		utxos = append(utxos, transformedUTXO)
	}

	return
}

func (c *UTXOClient) FetchUTXOsForAddressLessEqualThanAmount(amount int64, shardID uint32, address string) (
	utxos txmodels.UTXORows, err error) {
	// Note: no need to check arguments' validity.
	// This validation is delegated to the API itself.
	// In worst case - error would be returned.

	query := fmt.Sprint(c.baseURL, "less-equal/", "?address=", address, "&amount=", amount)

	var (
		statusCode int
		body       []byte
	)
	statusCode, body, err = c.client.Get(nil, query)
	if err != nil {
		return
	}

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("%d, %w", statusCode, ec.ErrInvalidIndexResponse)
		return
	}

	var receivedUTXOs []utxo.UTXO
	err = json.Unmarshal(body, &receivedUTXOs)
	if err != nil {
		return
	}

	for _, receivedUTXO := range receivedUTXOs {
		transformedUTXO := txmodels.UTXO{
			ShardID:    shardID,
			Address:    address,
			TxHash:     receivedUTXO.TxHash,
			OutIndex:   uint32(receivedUTXO.TxOutput),
			Value:      receivedUTXO.Amount,
			Used:       false,
			PKScript:   receivedUTXO.PKScript,
			ScriptType: "", //"pubkeyhash",
		}

		utxos = append(utxos, transformedUTXO)
	}

	return
}
