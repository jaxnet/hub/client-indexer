package abstract

import "fmt"

type APIClient struct {
	*UTXOClient
}

func NewAPIClient(url string, strategy string, sigFunc SignatureFunc) (c *APIClient, err error) {
	baseURL := fmt.Sprint(url, "/api/v1/indexes/utxos/non-spent/")
	client, err := NewAbstractUTXOClient(url, baseURL, strategy, sigFunc)
	if err != nil {
		return
	}

	c = &APIClient{
		UTXOClient: client,
	}
	return
}
