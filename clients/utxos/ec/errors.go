package ec

import "errors"

var (
	ErrInvalidIndexResponse = errors.New("invalid index response")
	ErrInvalidAmount        = errors.New("invalid amount")
	ErrInvalidAddressList   = errors.New("invalid addresses list")
	ErrInvalidStrategy      = errors.New("invalid utxos fetching strategy")
	ErrInvalidHashesList    = errors.New("invalid hashes list")
)
