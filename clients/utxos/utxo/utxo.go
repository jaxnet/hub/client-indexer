package utxo

type UTXO struct {
	TxHash   string `json:"hash"`
	TxOutput uint16 `json:"output"`
	Amount   int64  `json:"amount"`
	PKScript string `json:"pkScript"`
}
