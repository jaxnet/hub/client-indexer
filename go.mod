module gitlab.com/jaxnet/hub/client-indexer

go 1.17

require (
	github.com/valyala/fasthttp v1.22.0
	gitlab.com/jaxnet/jaxnetd v0.4.5
)
